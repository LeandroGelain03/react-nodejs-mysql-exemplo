﻿# NodejS

## Para rodar: 

- npm install

- node index.js


### Obs: Configurar no arquivo 'index.js' as configurações pessoais do banco de dados.

# ReactJS

## Instalação:

 - npm install

### obs: se aparecer que precisa usar:

 - npm audit fix

### Obs: Caso necessario utilizar:

- yarn build.

## Para rodar:

- npm start

# MySQL

## Configurar de acordo com seu sistema.

## Testar e configurar para o projeto exemplo:

- create database app_tutorial;

- use app_tutorial;

 - create table usuarios(
	nome varchar(100),
    sobrenome varchar(100),
    email varchar(200)
    );

 - INSERT INTO usuarios(nome, sobrenome, email) values('leandro','gelain','meuemail@gmail.com');

- select * from usuarios;

# Possiveis erros:

- Front-end não funcionar devido a estar desligado/não pegando o back-end ou banco.

- ({"code":"PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR","fatal":false}) --> Erro que indica que o banco não está linkando por algum motivo com a API.
